#!/bin/bash

DATE_FORMAT="%Y%m%d-%H%M%S"
SAME_NAME=0
EXTENSION="CR2"

for filename in *.$EXTENSION
do
   creation_date=$(date -r ${filename} +${DATE_FORMAT})
   new_name="${creation_date}_${SAME_NAME}.${EXTENSION}"
   while [ -f ${new_name} ];
   do
      SAME_NAME=$((SAME_NAME+1))
      new_name="${creation_date}_${SAME_NAME}.${EXTENSION}"
   done
   mv -n ${filename} ${new_name}
   SAME_NAME=0
done
